import argparse
import redis

# you could also give the ip address of the machine where redis server is present.
redis_server = redis.Redis("localhost")

def getKeyValue(key):
  
    key_value = redis_server.get(key)
    print("Key:", key, " Value:", key_value)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("key", help="Key name")
    args = parser.parse_args()
    getKeyValue(args.key)

        
